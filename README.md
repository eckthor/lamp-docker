# LAMP stack built with Docker Compose

* PHP 5.6.x
* Apache
* MySQL
* phpMyAdmin


## Installation

Clone this repository on your local computer, Run the `docker-compose up -d`.
